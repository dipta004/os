# Week 05
## No 1
```
B
S
B
B
B
S
```
Masih dipikirkan.

## No 2
| Possible | Virtual Address | Physical Address | Segment |
| -------- | --------------- | ---------------- | ------- |
| YES | 0x0010 0000 | 0x1000 0000 | text |
| NO | 0x0030 0000 | ----- | ---- |
| YES | 0x0010 FEDC | 0x1000 FEDC | text |
| YES | 0x0011 0000 | 0x1001 0000 | text |
| NO | 0x7FFF FFFF | ----- | ---- |
| YES | 0x0020 1234 | 0x2000 1234 | data |
| YES | 0x8000 FFFF | 0x8000 FFFF | stack |
Sumber foto papan kelas pak rms.

## No 3
```
a. 1234567890
b. 90
c. 0000005A
d. 0080005A
e. 12 bits
f. 20 bits
g. 00800
h. 05A
i. PTE INDEX = 0x00800 * 2 = 0x01000
PTE ADDRESS = 0x001 000 + 0x001 000 = 0x002 000
PTE = 0x7120
j. 7 valid
k. Physical Frame = 120
l. Physical Address = 0x120 05A
```
| Address (HEX)            | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | A | B | C | D | E | F |
| -------------            | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - |
| 120 050 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 00 | 5A | 00 | 00 |


